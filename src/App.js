import React, {Fragment} from 'react';
import Homepage from './homepage/homepage.js';
import AboutMe from './aboutMe/aboutMe.js';
// import Recently from './recently/recently.js';
import Photos from './photos/photos.js';

import './App.css';

function App() {
  return (
    <Fragment>
        <Homepage></Homepage>
        <AboutMe></AboutMe>
        {/* <Recently></Recently> */}
        <Photos></Photos>
    </Fragment>
  );
}

export default App;
