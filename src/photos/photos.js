import React, {Fragment} from 'react';
import ChinaMiniGallery from './components/chinaMiniGallery/chinaMiniGallery';
import JapanMiniGallery from './components/japanMiniGallery/japanMiniGallery';
import YellowstoneMiniGallery from './components/yellowstoneMiniGallery/yellowstonePhotoMiniGallery';
import IcelandMiniGallery from './components/icelandMiniGallery/icelandMiniGallery';
import TitleMiniGallery from './components/titleMiniGallery/titleMiniGallery';
import FooterMiniGallery from './components/footerMiniGallery/footerMiniGallery';

function photos() {
  return (
    <Fragment>
        <TitleMiniGallery></TitleMiniGallery>
        <ChinaMiniGallery></ChinaMiniGallery>
        <JapanMiniGallery></JapanMiniGallery>
        <YellowstoneMiniGallery></YellowstoneMiniGallery>
        <IcelandMiniGallery></IcelandMiniGallery>
        <FooterMiniGallery></FooterMiniGallery>
    </Fragment>
  );
}

export default photos;