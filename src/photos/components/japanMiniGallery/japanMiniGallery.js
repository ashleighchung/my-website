import React from 'react';
import JapanPhotoMiniGallery1 from './japanPhotoMiniGallery1.jpg';
import JapanPhotoMiniGallery2 from './japanPhotoMiniGallery2.jpg';
import JapanPhotoMiniGallery3 from './japanPhotoMiniGallery3.jpg';
import JapanPhotoMiniGallery4 from './japanPhotoMiniGallery4.jpg';
import JapanPhotoMiniGallery5 from './japanPhotoMiniGallery5.jpg';
import JapanPhotoMiniGallery6 from './japanPhotoMiniGallery6.jpg';

import '../miniGallery.scss';
import './japanMiniGallery.scss'

function japanMiniGallery() {
  return (
    <div className = "photoWrapper japanPhotoWrapper">
        <div className = "photoLeftSpace"></div>
        <div className = "titleMiniGallery example">
            <button className = "galleryTitle hover japanHover hover-1">Japan</button>
        </div>
        <div className = "photoMiniGallery">
            <div className = "minigalleryRow">
              <img className = "miniPhoto" src = {JapanPhotoMiniGallery1} alt = "japan is great"/>
              <img className = "miniPhoto" src = {JapanPhotoMiniGallery5} alt = "japan is great"/>
              <img className = "miniPhotoLast" src = {JapanPhotoMiniGallery3} alt = "japan is great"/>
            </div>
            <div className = "minigalleryRow">
              <img className = "miniPhoto" src = {JapanPhotoMiniGallery4} alt = "japan is great"/>
              <img className = "miniPhoto" src = {JapanPhotoMiniGallery2} alt = "japan is great"/>
              <img className = "miniPhotoLast" src = {JapanPhotoMiniGallery6} alt = "japan is great"/>
            </div>
        </div>
        <div className = "photoRightSpace"></div>
    </div>
  );
}

export default japanMiniGallery;