import React from 'react';
import IcelandPhotoMiniGallery1 from './icelandPhotoMiniGallery1.JPG';
import IcelandPhotoMiniGallery2 from './icelandPhotoMiniGallery2.JPG';
import IcelandPhotoMiniGallery3 from './icelandPhotoMiniGallery3.JPG';
import IcelandPhotoMiniGallery4 from './icelandPhotoMiniGallery4.JPG';
import IcelandPhotoMiniGallery5 from './icelandPhotoMiniGallery5.JPG';
import IcelandPhotoMiniGallery6 from './icelandPhotoMiniGallery6.JPG';

import '../miniGallery.scss';
import './icelandMiniGallery.scss'

function icelandMiniGallery() {
  return (
    <div className = "photoWrapper icelandPhotoWrapper">
        <div className = "photoLeftSpace"></div>
        <div className = "titleMiniGallery example">
            <button className = "galleryTitle hover icelandHover hover-1">Iceland</button>
        </div>
        <div className = "photoMiniGallery">
            <div className = "minigalleryRow">
              <img className = "miniPhoto" src = {IcelandPhotoMiniGallery1} alt = "japan is great"/>
              <img className = "miniPhoto" src = {IcelandPhotoMiniGallery4} alt = "japan is great"/>
              <img className = "miniPhotoLast" src = {IcelandPhotoMiniGallery3} alt = "japan is great"/>
            </div>
            <div className = "minigalleryRow">
              <img className = "miniPhoto" src = {IcelandPhotoMiniGallery5} alt = "japan is great"/>
              <img className = "miniPhoto" src = {IcelandPhotoMiniGallery2} alt = "japan is great"/>
              <img className = "miniPhotoLast" src = {IcelandPhotoMiniGallery6} alt = "japan is great"/>
            </div>
        </div>
        <div className = "photoRightSpace"></div>
    </div>
  );
}

export default icelandMiniGallery;