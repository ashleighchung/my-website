import React from 'react';
import './titleMiniGallery.scss'

function titleMiniGallery() {
  return (
    <div className = "photoTitleMiniGallery">My Travel Diary</div>
  );
}

export default titleMiniGallery;