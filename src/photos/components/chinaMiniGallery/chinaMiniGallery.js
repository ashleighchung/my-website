import React from 'react';
import ChinaPhotoMiniGallery1 from './chinaPhotoMiniGallery1.JPG';
import ChinaPhotoMiniGallery2 from './chinaPhotoMiniGallery2.JPG';
import ChinaPhotoMiniGallery3 from './chinaPhotoMiniGallery3.JPG';
import ChinaPhotoMiniGallery4 from './chinaPhotoMiniGallery4.JPG';
import ChinaPhotoMiniGallery5 from './chinaPhotoMiniGallery5.JPG';
import ChinaPhotoMiniGallery6 from './chinaPhotoMiniGallery6.JPG';

import '../miniGallery.scss';
import './chinaMiniGallery.scss';

function chinaMiniGallery() {
  return (
    <div className = "chinaPhotoWrapper photoWrapper">
        <div className = "photoLeftSpace"></div>
        <div className = "photoMiniGallery chinaPhotoMiniGallery">
            <div className = "minigalleryRow">
              <img className = "miniPhoto" src = {ChinaPhotoMiniGallery1} alt = "china is great"/>
              <img className = "miniPhoto" src = {ChinaPhotoMiniGallery2} alt = "china is great"/>
              <img className = "miniPhotoLast" src = {ChinaPhotoMiniGallery3} alt = "china is great"/>
            </div>
            <div className = "minigalleryRow">
              <img className = "miniPhoto" src = {ChinaPhotoMiniGallery4} alt = "china is great"/>
              <img className = "miniPhoto" src = {ChinaPhotoMiniGallery5} alt = "china is great"/>
              <img className = "miniPhotoLast" src = {ChinaPhotoMiniGallery6} alt = "china is great"/>
            </div>
        </div>
        <div className = "titleMiniGallery chinaTitleMiniGallery example">
            <button className = "galleryTitle hover chinaHover hover-1">Beijing, China</button>
        </div>
        <div className = "photoRightSpace"></div>
    </div>
  );
}

export default chinaMiniGallery;