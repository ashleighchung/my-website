import React from 'react';
import YellowstonePhotoMiniGallery1 from './yellowstonePhotoMiniGallery1.jpg';
import YellowstonePhotoMiniGallery2 from './yellowstonePhotoMiniGallery2.jpg';
import YellowstonePhotoMiniGallery3 from './yellowstonePhotoMiniGallery3.jpg';
import YellowstonePhotoMiniGallery4 from './yellowstonePhotoMiniGallery4.jpg';
import YellowstonePhotoMiniGallery5 from './yellowstonePhotoMiniGallery5.jpg';
import YellowstonePhotoMiniGallery6 from './yellowstonePhotoMiniGallery6.jpg';

import '../miniGallery.scss';
import './yellowstoneMiniGallery.scss';

function yellowstoneMiniGallery() {
  return (
    <div className = "yellowstonePhotoWrapper photoWrapper">
        <div className = "photoLeftSpace"></div>
        <div className = "photoMiniGallery chinaPhotoMiniGallery">
            <div className = "minigalleryRow">
              <img className = "miniPhoto" src = {YellowstonePhotoMiniGallery1} alt = "yellowstone is great"/>
              <img className = "miniPhoto" src = {YellowstonePhotoMiniGallery2} alt = "yellowstone is great"/>
              <img className = "miniPhotoLast" src = {YellowstonePhotoMiniGallery3} alt = "yellowstone is great"/>
            </div>
            <div className = "minigalleryRow">
              <img className = "miniPhoto" src = {YellowstonePhotoMiniGallery5} alt = "yellowstone is great"/>
              <img className = "miniPhoto" src = {YellowstonePhotoMiniGallery4} alt = "yellowstone is great"/>
              <img className = "miniPhotoLast" src = {YellowstonePhotoMiniGallery6} alt = "yellowstone is great"/>
            </div>
        </div>
        <div className = "titleMiniGallery chinaTitleMiniGallery example">
            <button className = "galleryTitle hover yellowstoneHover hover-1">The American West</button>
        </div>
        <div className = "photoRightSpace"></div>
    </div>
  );
}

export default yellowstoneMiniGallery;