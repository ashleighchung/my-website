import React, {Fragment} from 'react';
import Heading from './components/heading/heading';
import Welcome from './components/welcome/welcome';

function homepage() {
  return (
    <Fragment>
      <Heading></Heading>
      <Welcome></Welcome>
    </Fragment>
  );
}

export default homepage;