import React from 'react';
import Recently1 from './recently1.jpg';
import Recently2 from './recently2.jpg';
import Recently3 from './recently3.jpg';


import './recently.scss';

function recently() {

    const text = {
        title: "What have I been up to?",
        paragraph: "blahb blabh",
    }

  return (
      <div className= "recentlyWrapper">
            <p class = "recentlyTitle">{text.title}</p>
            <img src = {Recently1} alt="ble" className = "recentlyImage"/>
            <img src = {Recently3} alt = "efe" className = "recentlyImage"/>
            {/* <img src = {Recently2} alt = "as" className = "recentlyImage"/> */}
            <p class = "recentlyParagraph">{text.paragraph}</p>
     </div>
  );
}

export default recently;