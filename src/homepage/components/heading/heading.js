import React from 'react';
import Photos from '../../../photos/photos';
import './heading.scss';

function heading() {
  return (
    <div className = "heading">
        <p className = "name">ASHLEIGH CHUNG</p>
        <div className = "navigationBar">
          <button class = "navigationItem"><a className = "navHyperLink">artwork</a></button>  
          <button class = "navigationItem"><a className = "navHyperLink">designs</a></button>
          <button class = "navigationItem"><a className = "navHyperLink" href = {Photos} >photos</a></button>
        </div>
    </div>
  );
}

export default heading;