import React from 'react';
import ProfilePic from './profile.jpeg';

import './welcome.scss';
import '../../../keyframes/keyframes.scss';

function welcome() {
  return (
    <div className = "content">
        <div className = "introTextWrapper">
            <p className = "hiText fade-up">Hi, my name is Ashleigh!</p>
            <div className = "innerIntroTextWrapper">
                <p className = "welcomeText fade-in" >Welcome to my website.</p>
            </div>
        </div>
        <div className = "imageWrapper">
            <img className = "slide profilePic" src= {ProfilePic} alt="Profile Picture of Ashleigh Chung"/>
        </div>
    </div>
  );
}

export default welcome;


/*
Problems with this component:
    Picture Sliding Left problem for small screen widths
*/