import React from 'react';
import AboutMe1 from './aboutMe1.JPG';
import AboutMe2 from './aboutMe2.JPG';
import AboutMe3 from './aboutMe3.JPG';
import AboutMe4 from './aboutMe4.JPG';
import AboutMe5 from './aboutMe5.jpg';

import './aboutMe.scss';
import './../keyframes/keyframes.scss'

function aboutMe() {

    const text = {
        title: "A Little About Me",
        paragraph: "Hi! I am an aspiring front-end engineer who loves design and anything related to art and creativity. Whether it be drawing, photography, cinematography, calligraphy, applied arts, or graphic design, I love to dabble in different fields to experiment and get my toes wet. One of my currently-growing passions is front-end development, hence why I decided to create my own website where I can showcase my work! Thanks for stopping by :)"
    }

  return (
    <div className = "aboutMe ">
        <div className = "aboutMeLeft"></div>
        <div className = "aboutMeImageWrapper fade-up">
            <div className = "aboutMeColumn1">
                <img className = "aboutMeImage" src= {AboutMe1} alt="Ashleigh Chung at Iceland Mountain"/>
                <img className = "aboutMeImage" src= {AboutMe2} alt="Iceland Sheep"/>
                <img className = "aboutMeImage" src= {AboutMe3} alt="Ashleigh Chung Iceland Sunset"/>
            </div>
            <div className = "aboutMeColumn2">
                <img className = "aboutMeImage" src= {AboutMe4} alt="Ashleigh Chung Blue Lagoon"/>
                <img className = "aboutMeImage" src= {AboutMe5} alt="Ashleigh Chung Eating the Best Fish in the World"/>
            </div>
        </div>
        <div className = "aboutMeTextWrapper fade-up">
            <div className = "aboutMeText">
                <p className = "aboutMeTitle">{text.title}</p>
                <p className = "aboutMeParagraph" >{text.paragraph}</p>
            </div>
        </div>
        <div className = "aboutMeRight"></div>
    </div>
  );
}

export default aboutMe;